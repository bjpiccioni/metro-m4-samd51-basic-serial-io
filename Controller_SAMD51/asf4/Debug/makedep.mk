################################################################################
# Automatically-generated file. Do not edit or delete the file
################################################################################

atmel_start.c

Device_Startup\startup_samd51.c

Device_Startup\system_samd51.c

driver_init.c

examples\driver_examples.c

hal\src\hal_adc_sync.c

hal\src\hal_atomic.c

hal\src\hal_cache.c

hal\src\hal_delay.c

hal\src\hal_evsys.c

hal\src\hal_flash.c

hal\src\hal_gpio.c

hal\src\hal_i2c_m_sync.c

hal\src\hal_init.c

hal\src\hal_io.c

hal\src\hal_rand_sync.c

hal\src\hal_sleep.c

hal\src\hal_spi_m_sync.c

hal\src\hal_timer.c

hal\src\hal_usart_async.c

hal\src\hal_usb_device.c

hal\src\hal_wdt.c

hal\utils\src\utils_assert.c

hal\utils\src\utils_event.c

hal\utils\src\utils_list.c

hal\utils\src\utils_ringbuffer.c

hal\utils\src\utils_syscalls.c

hpl\adc\hpl_adc.c

hpl\cmcc\hpl_cmcc.c

hpl\core\hpl_core_m4.c

hpl\core\hpl_init.c

hpl\dmac\hpl_dmac.c

hpl\evsys\hpl_evsys.c

hpl\gclk\hpl_gclk.c

hpl\mclk\hpl_mclk.c

hpl\nvmctrl\hpl_nvmctrl.c

hpl\osc32kctrl\hpl_osc32kctrl.c

hpl\oscctrl\hpl_oscctrl.c

hpl\pm\hpl_pm.c

hpl\ramecc\hpl_ramecc.c

hpl\rtc\hpl_rtc.c

hpl\sercom\hpl_sercom.c

hpl\systick\hpl_systick.c

hpl\trng\hpl_trng.c

hpl\usb\hpl_usb.c

hpl\wdt\hpl_wdt.c

usb\class\cdc\device\cdcdf_acm.c

usb\device\usbdc.c

usb\usb_protocol.c

usb_cdc_echo_main.c

usb_start.c

