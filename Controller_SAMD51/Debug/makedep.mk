################################################################################
# Automatically-generated file. Do not edit or delete the file
################################################################################

asf4\atmel_start.c

asf4\Device_Startup\startup_samd51.c

asf4\Device_Startup\system_samd51.c

asf4\driver_init.c

asf4\hal\src\hal_adc_sync.c

asf4\hal\src\hal_atomic.c

asf4\hal\src\hal_cache.c

asf4\hal\src\hal_delay.c

asf4\hal\src\hal_evsys.c

asf4\hal\src\hal_flash.c

asf4\hal\src\hal_gpio.c

asf4\hal\src\hal_i2c_m_sync.c

asf4\hal\src\hal_init.c

asf4\hal\src\hal_io.c

asf4\hal\src\hal_rand_sync.c

asf4\hal\src\hal_sleep.c

asf4\hal\src\hal_spi_m_sync.c

asf4\hal\src\hal_timer.c

asf4\hal\src\hal_usart_async.c

asf4\hal\src\hal_usb_device.c

asf4\hal\src\hal_wdt.c

asf4\hal\utils\src\utils_assert.c

asf4\hal\utils\src\utils_event.c

asf4\hal\utils\src\utils_list.c

asf4\hal\utils\src\utils_ringbuffer.c

asf4\hal\utils\src\utils_syscalls.c

asf4\hpl\adc\hpl_adc.c

asf4\hpl\cmcc\hpl_cmcc.c

asf4\hpl\core\hpl_core_m4.c

asf4\hpl\core\hpl_init.c

asf4\hpl\dmac\hpl_dmac.c

asf4\hpl\evsys\hpl_evsys.c

asf4\hpl\gclk\hpl_gclk.c

asf4\hpl\mclk\hpl_mclk.c

asf4\hpl\nvmctrl\hpl_nvmctrl.c

asf4\hpl\osc32kctrl\hpl_osc32kctrl.c

asf4\hpl\oscctrl\hpl_oscctrl.c

asf4\hpl\pm\hpl_pm.c

asf4\hpl\ramecc\hpl_ramecc.c

asf4\hpl\rtc\hpl_rtc.c

asf4\hpl\sercom\hpl_sercom.c

asf4\hpl\systick\hpl_systick.c

asf4\hpl\trng\hpl_trng.c

asf4\hpl\usb\hpl_usb.c

asf4\hpl\wdt\hpl_wdt.c

asf4\usb\class\cdc\device\cdcdf_acm.c

asf4\usb\device\usbdc.c

asf4\usb\usb_protocol.c

asf4\usb_start.c

main.c

tinyusb-SAMD51\metro_m4_express.c

tinyusb-SAMD51\tusb-src\class\audio\audio_device.c

tinyusb-SAMD51\tusb-src\class\bth\bth_device.c

tinyusb-SAMD51\tusb-src\class\cdc\cdc_device.c

tinyusb-SAMD51\tusb-src\class\cdc\cdc_host.c

tinyusb-SAMD51\tusb-src\class\cdc\cdc_rndis_host.c

tinyusb-SAMD51\tusb-src\class\dfu\dfu_rt_device.c

tinyusb-SAMD51\tusb-src\class\hid\hid_device.c

tinyusb-SAMD51\tusb-src\class\hid\hid_host.c

tinyusb-SAMD51\tusb-src\class\midi\midi_device.c

tinyusb-SAMD51\tusb-src\class\msc\msc_device.c

tinyusb-SAMD51\tusb-src\class\msc\msc_host.c

tinyusb-SAMD51\tusb-src\class\net\net_device.c

tinyusb-SAMD51\tusb-src\class\usbtmc\usbtmc_device.c

tinyusb-SAMD51\tusb-src\class\vendor\vendor_device.c

tinyusb-SAMD51\tusb-src\class\vendor\vendor_host.c

tinyusb-SAMD51\tusb-src\common\tusb_fifo.c

tinyusb-SAMD51\tusb-src\dcd_samd.c

tinyusb-SAMD51\tusb-src\device\usbd.c

tinyusb-SAMD51\tusb-src\device\usbd_control.c

tinyusb-SAMD51\tusb-src\tusb.c

usb_descriptors.c

