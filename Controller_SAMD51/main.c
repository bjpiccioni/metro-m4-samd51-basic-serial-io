#include <string.h>
#include <ctype.h>

#include "board.h"
#include "tusb.h"
#include <atmel_start.h>

extern void board_led_write(bool state);

//
// These are the stdio routines for printf, getc, etc..
//
//
// Write a buffer to the virtual serial port 0
//
int _write (int fd, const void *buf, size_t count)
{
    (void)fd;
int cnt = (int) tud_cdc_n_write(0, buf, count );
    tud_cdc_n_write_flush(0);
    return( cnt );
}

//
// Read from the virtual serial port 0
//      returns 0 for no characters
//
int _read (int fd, const void *buf, size_t count)
{
    if ( tud_cdc_n_available( 0 ) == 0 ) 
        return( 0 );
    else
        return( (int) tud_cdc_n_read(0, (void *) buf, count ));
}

void    Putc( int port, char c )
{
uint8_t    buf[2];
    buf[0] = c;    
    tud_cdc_n_write( port, buf, 1 );
    tud_cdc_n_write_flush( port );
}

char    Getc( int port )
{
uint8_t    buf[2];

    buf[0] = 0;
    if ( tud_cdc_n_available( port ) != 0 ) 
         if( 0 != tud_cdc_n_read( port, (void *) buf, 1 ) )
            return( buf[0]);    

    return( 0 );    
}

static inline void    ConOut( uint8_t c ) { Putc( 0, c ); }
static inline void    AuxOut( uint8_t c ) { Putc( 1, c ); }
static inline uint8_t ConIn( void ) { return( Getc( 0 ) ); }
static inline uint8_t AuxIn( void ) { return( Getc( 1 ) ); }

char   ConGetEcho( )
{
char    inp = ConIn();
        if( inp != 0 )
            ConOut( inp );
        return( inp );        
}

char   AuxGetEcho( )
{
char    inp = AuxIn();
        if( inp != 0 )
            AuxOut( inp );
        return( inp );
}

extern uint32_t SECONDS;

int main(void)
{
uint32_t    prior_seconds = 0;
char        port0in, port1in;

    atmel_start_init();
    board_init();
    tusb_init();

    while (1)
    {
        tud_task(); // tinyusb device task
        port1in = AuxIn();
        if( port1in != 0 )
            ConOut( port1in );
                    
        port0in = ConIn();
        if( port0in != 0 )
            AuxOut( port0in );

        if( prior_seconds != SECONDS )
        {
            prior_seconds = SECONDS;
            board_led_write( ( SECONDS & 1 ) == 0 );
//            printf( "Test %ld\n", SECONDS );
        }
    }
}

